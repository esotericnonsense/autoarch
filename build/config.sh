#!/usr/bin/env bash
set -euxo pipefail

# The name of the generated ISO
export iso_name="autoarch"

# The working directory used to generate the ISO
export work_dir="/tmp/archiso-work"

# The final output directory of the generated ISO
export out_dir="/tmp/archiso-out"

# The ethernet interface used for networking on the live system
export iface="ens1"

# OPTIONAL: if set, clone this repository at runtime and use it to install
# if not set, build the local install directory into the ISO
# export install_repo="https://github.com/esotericnonsense/autoarch.git"
